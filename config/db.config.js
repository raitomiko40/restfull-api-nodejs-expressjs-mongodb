// config/db.config.js
const mongoose = require('mongoose');

async function connectToDatabase() {
  try {
    await mongoose.connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    console.log('Terhubung ke database');
  } catch (error) {
    console.error('Kesalahan koneksi database:', error.message);
    process.exit(1); 
  }
}

connectToDatabase();
