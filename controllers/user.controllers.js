const User = require('../models/user.models');

exports.getAllUser = async (req, res) => {
  try {
    const users = await User.find();
    res.status(200).json({
      status: 200,
      data: users,
    });
  } catch (error) {
    res.status(400).json({
      status: 400,
      message: 'Permintaan tidak valid',
      error: error.message,
    });
  }
};

exports.createUser = async (req, res) => {
  try {
    const { username, password, biodata } = req.body;

    const newUser = await User.create({
      username,
      password,
      biodata,
    });

    res.status(201).json({
      status: 201,
      data: newUser,
    });
  } catch (error) {
    res.status(400).json({
      status: 400,
      message: 'Permintaan tidak valid',
      error: error.message,
    });
  }
};

exports.getUserById = async (req, res) => {
  try {
    const { id } = req.params;
    
    const user = await User.findById(id);
    
    if (!user) {
      return res.status(404).json({
        status: 404,
        message: 'Pengguna tidak ditemukan',
      });
    }
    
    res.status(200).json({
      status: 200,
      data: user,
    });
  } catch (error) {
    res.status(500).json({
      status: 500,
      message: 'Terjadi kesalahan dalam mencari pengguna',
      error: error.message,
    });
  }
};

exports.updateUserById = async (req, res) => {
    try {
      const { id } = req.params;
      const { username, password, biodata } = req.body;
  
      // Mencari pengguna berdasarkan ID dalam database
      const user = await User.findById(id);
  
      // Memeriksa apakah pengguna dengan ID yang diberikan ditemukan
      if (!user) {
        return res.status(404).json({
          status: 404,
          message: 'Pengguna tidak ditemukan',
        });
      }
  
      // Mengupdate data pengguna
      user.username = username;
      user.password = password;
      user.biodata = biodata;
  
      // Menyimpan pengguna yang telah diperbarui ke database
      const updatedUser = await user.save();
  
      res.status(200).json({
        status: 200,
        data: updatedUser,
      });
    } catch (error) {
      res.status(500).json({
        status: 500,
        message: 'Terjadi kesalahan dalam mengupdate pengguna',
        error: error.message,
      });
    }
  };

  exports.deleteUserById = async (req, res) => {
    try {
      const { id } = req.params;
  
      // Menghapus pengguna berdasarkan ID dari database
      const result = await User.deleteOne({ _id: id });
  
      // Memeriksa apakah pengguna dengan ID yang diberikan ditemukan
      if (result.deletedCount === 0) {
        return res.status(404).json({
          status: 404,
          message: 'Pengguna tidak ditemukan',
        });
      }
  
      res.status(200).json({
        status: 200,
        message: 'Pengguna berhasil dihapus',
      });
    } catch (error) {
      res.status(500).json({
        status: 500,
        message: 'Terjadi kesalahan dalam menghapus pengguna',
        error: error.message,
      });
    }
  };
  