require('dotenv').config()
require('./config/db.config');
const express = require('express');
const app = express();
const cors = require('cors');
const apiRouter = require('./routes/v1')
const port = process.env.PORT || 3001;



app.use(cors());
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use("/api", apiRouter)





app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
