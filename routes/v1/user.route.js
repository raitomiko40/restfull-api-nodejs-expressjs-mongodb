const userController = require("../../controllers/user.controllers");
const userRouter = require("express").Router();

/**
 * @Routes "/api/v1/players"
 */

userRouter.get("/", userController.getAllUser);
userRouter.post("/create", userController.createUser);
userRouter.get("/:id", userController.getUserById);
userRouter.put("/update/:id", userController.updateUserById);
userRouter.delete("/delete/:id", userController.deleteUserById);
// userRouter.post("/exp/:id", userController.updateExperience);

module.exports = userRouter;
