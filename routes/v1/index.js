const userRoute = require('./user.route')
const v1 = require("express").Router();

v1.get("/", (_, res) => {
  res.send("from v1");
});

v1.use("/users", userRoute);

module.exports = v1;
